import { AComponent } from './foo/a/a.component';
import { BComponent } from './foo/b/b.component';
import { CComponent } from './foo/c/c.component';

export interface ComponentDesc {
  id: string;
  label: string;
  component: any;
}

export function getDynamicComponents(): ComponentDesc[] {
  return [
    {
      id: '1',
      label: 'A (dynamic)',
      component: AComponent
    },
    {
      id: '2',
      label: 'B (dynamic)',
      component: BComponent
    },
    {
      id: '3',
      label: 'C (dynamic)',
      component: CComponent
    }
  ]
}
