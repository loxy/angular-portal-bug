import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ComponentDesc, getDynamicComponents } from './dynamic-component-list';

@Injectable({
  providedIn: 'root'
})
export class ActiveComponentsSimpleService {
  private components = new BehaviorSubject<ComponentDesc[]>(null);

  constructor() {
    const dynamicComponents = getDynamicComponents();
    this.components.next(dynamicComponents);
  }

  selectComponents() {
    return this.components.asObservable();
  }
}
