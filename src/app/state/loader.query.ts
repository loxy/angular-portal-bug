import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { LoaderStore, LoaderState } from './loader.store';

@Injectable({ providedIn: 'root' })
export class LoaderQuery extends QueryEntity<LoaderState> {

  constructor(protected store: LoaderStore) {
    super(store);
  }
}
