import { Injectable } from '@angular/core';
import { ComponentDesc } from '../dynamic-component-list';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export interface LoaderState extends EntityState<ComponentDesc> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'loader' })
export class LoaderStore extends EntityStore<LoaderState> {

  constructor() {
    super();
  }

}

