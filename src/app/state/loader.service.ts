import { Injectable } from '@angular/core';
import { getDynamicComponents } from '../dynamic-component-list';
import { LoaderQuery } from './loader.query';
import { LoaderStore } from './loader.store';

@Injectable({ providedIn: 'root' })
export class LoaderService {

  constructor(private store: LoaderStore,
              private query: LoaderQuery) {
    // *** this line
    store.set(getDynamicComponents());

    // *** leads to
    // Cannot assign to read only property 'tView' of object '[object Object]'
    // at getOrCreateTComponentView (core.js:12009)
    // at createRootComponentView (core.js:25985)
    // at ComponentFactory$1.create (core.js:33511)
    // at ViewContainerRef.createComponent (core.js:15380)
    // at CdkPortalOutlet.attachComponentPortal (portal.js:823)
    // at CdkPortalOutlet.attach (portal.js:330)
    // at CdkPortalOutlet.set portal [as portal] (portal.js:778)
    // at setInputsForProperty (core.js:13576)
    // at elementPropertyInternal (core.js:12391)
    // at Module.ɵɵproperty (core.js:20914)
  }

  selectAll() {
    return this.query.selectAll();
  }
}
