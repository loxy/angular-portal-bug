import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AComponent } from './a/a.component';
import { BComponent } from './b/b.component';
import { CComponent } from './c/c.component';

@NgModule({
  declarations: [
    AComponent,
    BComponent,
    CComponent
  ],
  imports: [
    CommonModule
  ]
})
export class FooModule {
}
