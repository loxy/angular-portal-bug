import { Component, OnInit } from '@angular/core';
import { SomeService } from '../../some.service';

@Component({
  selector: 'app-c',
  templateUrl: './c.component.html',
  styleUrls: ['./c.component.css']
})
export class CComponent implements OnInit {

  constructor(private service: SomeService) {
  }

  ngOnInit(): void {
    console.log(this.service.doSomething());
  }

}
