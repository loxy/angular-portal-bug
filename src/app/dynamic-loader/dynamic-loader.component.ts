import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { Component, OnInit } from '@angular/core';
import { ActiveComponentsSimpleService } from '../active-components-simple.service';
import { ComponentDesc } from '../dynamic-component-list';
import { LoaderService } from '../state/loader.service';

@Component({
  selector: 'app-dynamic-loader',
  templateUrl: './dynamic-loader.component.html',
  styleUrls: ['./dynamic-loader.component.css']
})
export class DynamicLoaderComponent implements OnInit {
  selectedPortal: Portal<any>;
  componentPortal: ComponentPortal<any>[] = [];
  components: ComponentDesc[];

  constructor(private service: ActiveComponentsSimpleService, private akita: LoaderService) {
  }

  ngOnInit(): void {
    this.useNormalSubject();
    // this.useAkita();
  }

  private useAkita() {
    this.akita.selectAll().subscribe(list => {
      this.cleanPortalList();
      this.components = list;
      for (const c of list) {
        console.log(c);
        this.componentPortal.push(new ComponentPortal(c.component));
      }
      if (this.componentPortal.length > 0) {
        this.selectedPortal = this.componentPortal[0];
      }
    });
  }

  private useNormalSubject() {
      this.service.selectComponents().subscribe(list => {
        this.cleanPortalList();
        this.components = list;
        for (const c of list) {
          console.log(c);
          this.componentPortal.push(new ComponentPortal(c.component));
        }
        if (this.componentPortal.length > 0) {
          this.selectedPortal = this.componentPortal[0];
        }
      });
  }

  switchTo(index: number) {
    this.selectedPortal = this.componentPortal[index];
  }

  private cleanPortalList() {
    for (const c of this.componentPortal) {
      c.detach();
    }
    this.componentPortal = [];
  }
}
